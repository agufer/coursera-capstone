import { Typography, Stack, Box } from "@mui/material";
import TestimonialCard from "../elementcomponent/TestimonialCard";
import SectionBody from "../../smallcomponents/SectionBody";

const Testimonial = () => {
    const sectionClass = "my-6 py-12 relative flex flex-col justify-content-center"
    const testimonials = [
        {
            stars: 4,
            name: 'Francis',
            review: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam odit, repellat accusantium provident adipisci facilis molestiae deserunt voluptas.'
        },
        {
            stars: 4,
            name: 'Loreane',
            review: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam odit, repellat accusantium provident adipisci facilis molestiae deserunt voluptas.'
        },
        {
            stars: 5,
            name: 'Madeline',
            review: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam odit, repellat accusantium provident adipisci facilis molestiae deserunt voluptas.'
        },
        {
            stars: 5,
            name: 'Juseppe',
            review: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam odit, repellat accusantium provident adipisci facilis molestiae deserunt voluptas.'
        },
    ]

    return (
        <SectionBody>
            <Box bgcolor="primary.main" sx={{top: 0, left: '50%', transform: "translateX(-50%)"}} className="absolute vw-100 h-100" />
            <Box className="my-auto">
                <Stack direction="row" className="justify-content-center relative z-10 pb-6">
                    <Typography variant="h3" color="white.main" className="std-text">Testimonials</Typography>
                </Stack>
                <Stack direction="row" className="py-6 relative z-10">
                    {testimonials.map((testimonial) => {
                        return <TestimonialCard stars={testimonial.stars} review={testimonial.review} name={testimonial.name} key={testimonial.name}/>
                    })}
                </Stack>
            </Box>
        </SectionBody>
    );
};
export default Testimonial;
