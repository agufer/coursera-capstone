import { Stack, Typography} from "@mui/material";
import ButtonComp from "../../smallcomponents/Button";
import DishCard from "../elementcomponent/DishCard";
import SectionBody from "../../smallcomponents/SectionBody";

import desertPhoto from "../../../assets/lemondessert.jpg"
import greekPhoto from "../../../assets/greeksalad.jpg"
import bruchettaPhoto from "../../../assets/bruchetta.svg"

const Highlights = () => {
    const dishData = [
        {
            dish: 'Greek Salad',
            dishPrice: '$ 12.99',
            image: greekPhoto,
            detail: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus, fugiat totam! Quia quis temporibus maxime inventore, sint excepturi placeat, neque reprehenderit qui dolorem quae dolore mollitia incidunt officiis deserunt maiores!'
        },
        {
            dish: 'Bruchetta',
            dishPrice: '$ 5.99',
            image: bruchettaPhoto,
            detail: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus, fugiat totam! Quia quis temporibus maxime inventore, sint excepturi placeat, neque reprehenderit qui dolorem quae dolore mollitia incidunt officiis deserunt maiores!'
        },
        {
            dish: 'Lemon Dessert',
            dishPrice: '$ 5.00',
            image: desertPhoto,
            detail: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Repellendus, fugiat totam! Quia quis temporibus maxime inventore, sint excepturi placeat, neque reprehenderit qui dolorem quae dolore mollitia incidunt officiis deserunt maiores!'
        },
    ]
    return (
        <SectionBody>
            <Stack direction="row" className="justify-content-between">
                <Typography variant="h3" color="black.main" className="std-text mb-3">This weeks specials!</Typography>
                <ButtonComp variant="contained" color="secondary">
                    Online menu
                </ButtonComp>
            </Stack>
            <Stack direction="row" className="pt-12">
                {dishData.map((dish) => {
                    return <DishCard dish={dish.dish} image={dish.image} dishprice={dish.dishprice} detail={dish.detail} key={dish.dish}/>
                })}
            </Stack>
        </SectionBody>
    );
};
export default Highlights;
