import { Stack, Box, Typography } from "@mui/material";
import foto from "../../../assets/marioadrianA.jpg"
import fotoB from "../../../assets/marioadrianB.jpg"
import SectionBody from "../../smallcomponents/SectionBody";

const About = () => {
    return (
        <SectionBody>
            <Stack direction="row" className="py-9">
                <Box className="w-100 z-10">
                    <Typography variant="h2" color="secondary" className="dt-text">Little Lemmon</Typography>
                    <Typography variant="h3" color="black.main" className="tt-text mb-3">Chicago</Typography>
                    <Typography color="black.main" className="tt-text mb-3 pr-9">Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus blanditiis rerum, reiciendis aut enim commodi totam voluptatem facilis? Repellat exercitationem similique, dignissimos maiores nam quae adipisci aut perferendis asperiores sed?</Typography>
                </Box>
                <Box className="w-100 d-flex">
                    <Box className="w-100 ms-auto">
                        <Box sx={{paddingBottom: 'calc(100% * .75/1)'}} className="relative">
                            <img
                                src={foto} style={{border: 'solid 0px', overflow: 'hidden', borderRadius: '22px', top: 0, right:0}} 
                                className="w-75 h-75 object-cover absolute"
                                alt=""
                                loading="lazy"
                            />
                            <img
                                src={fotoB} style={{border: 'solid 0px', overflow: 'hidden', borderRadius: '22px', bottom: 0, left:0}} 
                                className="w-75 h-75 object-cover absolute"
                                alt=""
                                loading="lazy"
                            />
                        </Box>
                    </Box>
                </Box>
            </Stack>
        </SectionBody>
    );
};
export default About;
