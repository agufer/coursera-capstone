import { Stack, Box, Typography } from "@mui/material";
import ButtonComp from "../../smallcomponents/Button";
import foto from "../../../assets/restauranfood.jpg"
import { NavLink } from "react-router-dom";

const Hero = () => {
    return (
        <Box component="section">
            <Stack direction="row" className="py-12 relative">
                <Box bgcolor="primary.main" sx={{top: 0, left: '50%', transform: "translateX(-50%)"}} className="absolute vw-100 h-100" />
                <Box className="w-100 z-10">
                    <Typography variant="h2" color="secondary" className="dt-text">Little Lemmon</Typography>
                    <Typography variant="h3" color="white.main" className="tt-text mb-3">Chicago</Typography>
                    <Typography color="white.main" className="tt-text mb-3">We are a family owned Mediterranean restaurant, focused on traditional recipes served with a modern twist.</Typography>
                    <NavLink to="/reservations">
                        <ButtonComp variant="contained" color="secondary">
                            Reserve a table
                        </ButtonComp>
                    </NavLink>
                </Box>
                <Box className="w-100 d-flex">
                    <Box className="w-75 ms-auto">
                        <Box sx={{paddingBottom: 'calc(100% * 1/1)'}} className="relative">
                            <img
                                src={foto} style={{border: 'solid 0px', overflow: 'hidden', borderRadius: '22px', top: 0, left:0}} 
                                className="w-100 h-100 object-cover absolute"
                                alt=""
                                loading="lazy"
                            />
                        </Box>
                    </Box>
                </Box>
            </Stack>
        </Box>
    );
};
export default Hero;
