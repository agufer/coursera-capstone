import { Card, Stack, Avatar, Typography } from "@mui/material";
import StarIcon from '@mui/icons-material/Star';
import React from 'react'
import facePhoto from "../../../assets/face.png"

const TestimonialCard = (props) => {
    let endStars = []
    if (props.stars) {
        for (let index = 0; index < props.stars; index++) {
            endStars.push(<StarIcon color="secondary" key={index + 'a'}/>)
        }
    }
    return (
        <Card {...props} sx={{ minWidth: 200,maxWidth: 350 }} className="mx-3 w-fit px-6 py-3">
            <Stack direction="row" className="justify-content-start">
                {endStars.map((star) => {
                    return star
                })}
            </Stack>
            <Stack direction="row" className="justify-content-start py-3">
                <Avatar
                alt={props.name}
                src={facePhoto}
                sx={{ width: 48, height: 48 }}
                />
                <Typography gutterBottom variant="h5" component="h5" className="ps-3 my-auto" sx={{fontWeight: 'bold'}}>
                    {props.name}
                </Typography>
            </Stack>
            <Typography variant="body2" color="black.main">
                {props.review}
            </Typography>
        </Card>
    );
};
export default TestimonialCard;
