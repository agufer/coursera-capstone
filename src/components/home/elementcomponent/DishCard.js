import { Card, CardContent, Typography, CardActions, Stack } from "@mui/material";
import React from 'react'
import ButtonComp from "../../smallcomponents/Button";

const DishCard = (props) => {
    return (
        <Card {...props} sx={{ minWidth: 275, width: '100%', maxWidth: 350 }} className="mx-3">
        <img
            src={props.image} alt={props.dish} style={{border: 'solid 0px', overflow: 'hidden', height: 150}}
            className="object-cover w-100"
        />
        <CardContent className="pb-0">
            <Stack direction="row" className="justify-content-between">
                <Typography gutterBottom variant="h5" component="h5" sx={{fontWeight: 'bold'}}>
                    {props.dish}
                </Typography>
                <Typography gutterBottom variant="h5" color="secondary" component="h5">
                    {props.dishprice}
                </Typography>
            </Stack>
          <Typography variant="body2" color="text.secondary">
            {props.detail}
          </Typography>
        </CardContent>
        <CardActions className="px-3">
          <ButtonComp variant="text">Order a delivery</ButtonComp>
        </CardActions>
      </Card>
    );
};
export default DishCard;
