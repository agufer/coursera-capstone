import About from './sections/About';
import Hero from './sections/Hero';
import Highlights from './sections/Highlights';
import Testimonial from './sections/Testimonial';

function Home() {
  return (
    <>
        <Hero />
        <Highlights />
        <Testimonial />
        <About />
    </>
  )
}

export default Home;
