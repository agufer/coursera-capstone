import { Button } from "@mui/material";

const ButtonComp = (props) => {
    return (
        <Button  variant="contained" color="primary" {...props} className={"h-fit " + props?.className} sx={{ fontWeight: 'bold', ...props?.sx }}>
        </Button>
    );
};
export default ButtonComp;
