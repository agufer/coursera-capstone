import { Box } from "@mui/material";

const SectionBody = (props) => {
    return (
        <Box component="section" style={{minHeight: '60vh', height: 'fit-content'}} className="py-12 relative flex flex-column justify-content-center">
            {props.children}
        </Box>
    );
};
export default SectionBody;
