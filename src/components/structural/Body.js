import { Container } from "@mui/material";
import { Outlet  } from "react-router-dom";
import Navbar from "./Navbar";
import Footer from "./Footer";

function Body(props) {
  return (<>
    <Navbar />
    <Container style={{paddingTop: 64}} component="main">
        <Outlet />
        <Footer />
    </Container>
  </>

  )
}

export default Body;
