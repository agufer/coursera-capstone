import { AppBar, Toolbar, Box, Container } from "@mui/material";
import ButtonComp from "../smallcomponents/Button";
import { NavLink, useLocation } from "react-router-dom";
import Logo from "../../assets/Logo.svg"

const Navbar = () => {
    const location = useLocation().pathname

    const navItems = [{
        name: 'Home',
        route: "/"
    },{
        name: 'About',
    },     {
        name: 'Menu',
    }, {
        name: 'Reservation',
        route: "/reservations"
    },{
        name: 'Order online',
    },{
        name: 'Login',
    }]

    function variantSelector(route) {
        switch (route === location) {
            case true: return 'contained'
            default: return 'text'
        }
    }

    return (
        <AppBar elevation={0} position="fixed" color="white" component="nav">
            <Toolbar>
                <Container className="d-flex justify-content-between">
                    <Box className="mx-2">
                        <img
                            src={Logo}
                            alt=""
                            loading="lazy"
                        />
                    </Box>
                    <Box>
                        {navItems.map((item) => (
                            <NavLink to={item.route} key={item.name} className="mx-2">
                                <ButtonComp variant={variantSelector(item.route)}>
                                    {item.name}
                                </ButtonComp>
                            </NavLink>
                        ))}
                    </Box>
                </Container>
            </Toolbar>
        </AppBar>
    );
};
export default Navbar;
