import { Typography, Stack, Box } from "@mui/material";
import logo from "../../assets/VectorLogo.svg"
import { NavLink } from "react-router-dom";

const Footer = () => {
    const sectionClass = "py-12 relative flex flex-col justify-content-center"
    const elementTitleClass = "st-text w-fit mb-2"
    const elementLinkClass = "lt-text w-fit mb-1 cursor-pointer" 
    return (
        <Box component="section" sx={{minHeight: 'fit-content'}} className={sectionClass}>
            <Box bgcolor="primary.main" sx={{top: 0, left: '50%', transform: "translateX(-50%)"}} className="absolute vw-100 h-100" />
            <Box className="my-auto">
                <Stack direction="row" className="justify-content-around relative z-10">
                    <Box className="px-4">
                        <img
                            src={logo} style={{width: 100, border: '0px solid', borderRadius: 5}}
                            className="object-cover h-100"
                            alt=""
                            loading="lazy"
                        />
                    </Box>
                    <Stack direction="column" className="px-4">
                        <Typography color="white.main" className={elementTitleClass}>Doormat Navigation</Typography>
                        <NavLink to="/">
                            <Typography color="white.main" className={elementLinkClass}>Home</Typography>
                        </NavLink>
                        <Typography color="white.main" className={elementLinkClass}>About</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Menu</Typography>
                        <NavLink to="/reservations">
                            <Typography color="white.main" className={elementLinkClass}>Reservations</Typography>
                        </NavLink>
                        <Typography color="white.main" className={elementLinkClass}>Order Online</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Login</Typography>
                    </Stack>
                    <Stack direction="column" className="px-4">
                        <Typography color="white.main" className={elementTitleClass}>Contact</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Adress</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Phone number</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Email</Typography>
                    </Stack>
                    <Stack direction="column" className="px-4">
                        <Typography color="white.main" className={elementTitleClass}>Social Media</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Adress</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Phone number</Typography>
                        <Typography color="white.main" className={elementLinkClass}>Email</Typography>
                    </Stack>
                </Stack>
            </Box>
        </Box>
    );
};
export default Footer;
