import { createBrowserRouter } from "react-router-dom";
import Home from "../components/home/Page";
import Reservation from "../components/reservation/Page";
import Body from "../components/structural/Body";

/* ********************************************** */

const Router = createBrowserRouter([
    {
        path: '/',
        element: <Body />,
        children: [
            {
                path: '/',
                element: <Home />,
            },
            {
                path: '/reservations',
                element: <Reservation />,
            },
        ]
    },
])

export default Router 