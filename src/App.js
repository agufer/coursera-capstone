import { Container } from "@mui/material";
import { ThemeProvider, createTheme } from '@mui/material/styles';
import Router from "./router"
import { RouterProvider } from "react-router-dom";

const theme = createTheme({
  palette: {
    tonalOffset: 0.125,
    mode: 'light',
    primary: {
      main: '#495E57',
    },
    secondary: {
      main: "#F4CE14"
    },
    white: {
      main: '#fff'
    },
    black: {
      main: '#000'
    }
  },
});


function App() {
  return (
    <ThemeProvider theme={theme}>
      <Container fixed>
        <RouterProvider router={Router} />
      </Container>
    </ThemeProvider>
  )
}

export default App;
