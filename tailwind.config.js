/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js,jsx,ts,tsx}"],
  theme: {
    screens: {
      'sm': '600px',
      // => @media (min-width: 600px) { ... }

      'md': '900px',
      // => @media (min-width: 900px) { ... }

      'lg': '1200px',
      // => @media (min-width: 1200px) { ... }

      'xl': '1500px',
      // => @media (min-width: 1500px) { ... }
      'xl2': '1800px',
      // => @media (min-width: 1800px) { ... }
    }
  },
  plugins: [],
}